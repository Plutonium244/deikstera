<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
                <div class="container-fluid">
                    <form action="{{ route('gen') }}" method="GET" >
                            <button type="gen" class="btn btn-default">
                                <i class="fa fa-plus"></i> Генерировать массив
                            </button>
                            </form>
                    <form action="{{ route('calculate')}}" method="GET" >
                                {{ csrf_field() }}
                    <input name="jsoned" type="hidden" value="{{json_encode($jsoned)}}">
                    <input name="size" type="hidden" value="{{$cities}}">
                    <table width="100%" cellspacing="0" cellpadding="5">
                    <tr>
                    <td width="200" valign="top">
                            @if(isset($jsoned))
                                <?php
                                    echo("Соединения:\n");
                                    $i = 0;
                                    foreach ($jsoned as $link) {
                                        $j = 0;
                                        foreach ($link as $sublink) {
                                            ?> <div class="row"> <?php
                                            if($i != $j) {
                                                $output = 'Из:'.$i.' В:'.$j.' Вес:'.$sublink;
                                                echo($output);
                                            }
                                            $j++;
                                        }
                                        $i++;
                                    }
                                    ?> <div class="row" >Городов всего: {{$cities}}
                            @endif                        </td>
                        <td valign="top">
                                <div class="form-group row">
                                    Из точки:
                                    <input type="number" class="form-control" name="from" min=0 max={{$cities-1}}>
                                </div>
                                <div class="form-group row">
                                    В точку:
                                    <input type="number" class="form-control" name="to" min=0 max={{$cities-1}}>
                                </div>
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-plus"></i> Вычислить путь
                                </button>
                            </form>
                        </td>
                    </tr>
                    </table>
                    </div>
                </div>
        </div>
    </body>
</html>
