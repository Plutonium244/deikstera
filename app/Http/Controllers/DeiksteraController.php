<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Way;

class DeiksteraController extends Controller
{
    public function gen() // генератор случайного массива соединений. НРесуществующие пути будут помечены как 0.
    {
        $jsoned;
        $cities = rand(4, 6);
        for ($i = 0; $i < $cities; $i++) {
            for ($j = $i; $j < $cities; $j++) {
                    $link = rand(0, 7);
                    if ($j!= $i) {
                        $jsoned[$i][$j] = $link;
                        $jsoned[$j][$i] = $link;
                    } else {
                        $jsoned[$i][$j] = 0;
                    }
            }
        }
        return view('deikstera', [
            'jsoned' => $jsoned,
            'cities' => $cities,
        ]);
    }

    public function calculate(Request $request)
    {
        $start = $request['from'];
        $from[] = $request['from'];
        $to = intval($request['to']);
        $jsoned = json_decode($request['jsoned']);
        $count = $request['size'];
        $checked[] = $request['from']; // добавим исходную как проверенную, чтоб не возвращаться к ней.
        $ways = [[]]; // будем хранить пути как путь от точки А (1-я коорд массива) до точки Б (2-я коорд массива)
        $minLenght = 500;
        do {
            $j = 0;
            $currentway = isset($ways[$start][$from[0]])
                ?$ways[$start][$from[0]]
                :0;
            if (array_search($from[0], $checked) == null) {// Если эта точка не отмечена как пройденная
                foreach ($jsoned[$from[0]] as $link) { // проверяем все выходы из этой точки
                    if ($jsoned[$from[0]][$j] != 0) { // Если есть соединение
                        if (!isset($ways[$start][$j])) { // Если такого пути нет в массиве, то установим
                            $ways[$start][$j] = $currentway + $link;
                        }
                        if ($ways[$start][$j] > $currentway + $link) { // Если путь есть, но мы нашли короче - установим короткий.
                            $ways[$start][$j] = $currentway + $link;
                        }
                        if ($j == $to && $currentway + $link < $minLenght) { // Если это конечная точка маршрута и если меньше, чем раньше найденное, то установим
                            $minLenght = $currentway + $link;
                        }
                        array_search($j, $checked) == null
                            ?$from[] = $j
                            :null; // добавим пути для последующих обходов. Кроме уже проверенных.
                    // dump($from);
                    }
                    $j++;
                }
                $checked[] = $from[0]; // добавим метку, что вершина пройдена
            }
            array_shift($from); // проверили вершину, выкинем
        } while (isset($from[0]));
        if ($minLenght == 500) {
            dd('Не удалось найти путь. Перестройте граф');
        }
        dump('Я закончил искать кратчайшее расстояние! ', $minLenght, ' метров!');
        $shortestRoute = strval($to);
        $tempMinLength = $minLenght;
        do {
            for ($i = 0; $i < $count; $i++) {
                if ($jsoned[$i][$to] > 0 && $i != $to) {
                    $prevDistance = $tempMinLength - $jsoned[$i][$to];
                    if (isset($ways[$start][$i])) {
                        if ($prevDistance == $ways[$start][$i]) {
                            $tempMinLength -= $jsoned[$i][$to];
                            $shortestRoute .= ' ,';
                            $shortestRoute .= strval($i);
                            $to = $i;
                        }
                    }
                }
                if ($tempMinLength - $jsoned[$to][$i] == 0 && $i == $start) {
                    $shortestRoute .= ' ,';
                    $shortestRoute .= $start;
                    break(2);
                }
            }
        }
        while ($tempMinLength > 0);
        dd('Через точки: ', strrev($shortestRoute));
        return view('deikstera', [
            'cities' => 9999,
        ]);
    }
}
